﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatoCombos : VariablesG
{
    public Animator animator;
    
    int comboStatus = 0;
    void Update()
    {
        if (Input.GetKeyDown("q"))
        {
            StartCoroutine(combo("q", 0.5f));
        }
        else if (Input.GetKeyDown("e"))
        {
            StartCoroutine(combo("e", 0.5f));
        }
    }
    IEnumerator combo(string s, float wait)
    {
        if (comboStatus == 0)
        {
            if (s == "q")
            {
                // print("Q");
                animator.SetTrigger("Atack1");
                comboStatus = 1;//q
                yield return new WaitForSeconds(wait);
                if (comboStatus == 1)
                {                    
                    comboStatus = 0;
                }
            }
            if (s == "e")
            {
                //print("E");
                animator.SetTrigger("Atack2");
                comboStatus = 1;//e
                yield return new WaitForSeconds(wait);
                if (comboStatus == 1)
                {                    
                    comboStatus = 0;
                }
            }
        }       
    }
}
