﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private HealthSystem healthSystem;

    public void Setup(HealthSystem healthSystem)
    {
        //subscripcion al evento
        this.healthSystem = healthSystem;
        healthSystem.OnHealthChange += HealthSystem_OnHealthChanger;
    }
   
    private void HealthSystem_OnHealthChanger(object sender, EventArgs e)
    {
        transform.Find("Bar").localScale = new Vector3(healthSystem.GetHealthPercent(), 1);        
    }
    
}
