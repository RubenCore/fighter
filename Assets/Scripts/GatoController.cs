﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatoController : VariablesG
{
    //movimiento del pj ,si el pj quiere dos saltos cambiar el contador de saltos a mas
    private float Pjmove = 0f;
    private Rigidbody2D m_rb;
    //velocidad
    private float PjVel = 10f;
    // private bool salto = false;
    public int saltoCount = 2;
    private bool m_FacingRight = true;
    //rotacion de particulas
    public ParticleSystem ps;
    private Vector3 rotationDere = new Vector3(0.0f, 80f, 0.0f);
    private Vector3 rotationIzqi = new Vector3(0.0f, 278f, 0.0f);
    private bool saltando = true;
    //animacion
    public Animator animator;
    //barra de vida
    public HealthBar healthBar;
    public HealthSystem healthSystem;


    void Start()
    {
        m_rb = GetComponent<Rigidbody2D>();
        //pongo la barra de vida a 60
        healthSystem = new HealthSystem(60);
        healthBar.Setup(healthSystem);
    }
    void Update()
    {
        if (healthSystem.GetHealth() == 0)
        {
            //si la barra de vida es 0 vuelve a esta posicion con la vida a 60
            this.GetComponent<Rigidbody2D>().position = new Vector2(-9f, -10f);
            healthSystem.Heal(60);
        }
        Pjmove = Input.GetAxisRaw("Horizontal");

        //control de las particulas al correr
        if (Pjmove == -1 && !saltando)
        {
            ps.Play();
            var shape = ps.shape;
            shape.rotation = rotationDere;
        }
        else if (Pjmove == -1 && saltando)
        {
            ps.Pause();
            ps.Clear();
        }
        else if (Pjmove == 1 && !saltando)
        {
            ps.Play();
            var shape = ps.shape;
            shape.rotation = rotationIzqi;

        }
        else if (Pjmove == 1 && saltando)
        {
            ps.Pause();
            ps.Clear();
        }
        else if (Pjmove == 0)
        {
            ps.Pause();
            ps.Clear();
        }
        //si pulsas w o UpArrow y el contador de salto es mayor a 0 saltas con una fuerza y se activa la animacion saltando
        if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) && saltoCount > 0)
        {
            m_rb.AddForce(new Vector2(0, 450));
            saltoCount--;
            saltando = true;
            animator.SetBool("saltando", true);
        }

       animator.SetFloat("PjVel", Mathf.Abs(Pjmove));

    }
    void FixedUpdate()
    {
        move(Pjmove);
    }
    //Movimiento del pj
    private void move(float move)
    {
        m_rb.velocity = new Vector2(PjVel * move, m_rb.velocity.y);
        if (move == 1 && !m_FacingRight)
        {
            Vector3 Scale = transform.localScale;
            Scale.x *= -1;
            transform.localScale = Scale;
            m_FacingRight = true;
        }
        if (move == -1 && m_FacingRight)
        {
            Vector3 Scale = transform.localScale;
            Scale.x *= -1;
            transform.localScale = Scale;
            m_FacingRight = false;
        }

    }
    //desactivo la animacion de saltando
    public void Aterrizando()
    {
        animator.SetBool("saltando", false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //miro si ha colisionado con el suelo para devolver a 1 el contador de salto y desactivo la animacion de salto con la funcion Aterrizando
        if (collision.transform.tag == "Suelo")
        {
            saltoCount = 2;
            saltando = false;
            Aterrizando();
        }

    }


}
