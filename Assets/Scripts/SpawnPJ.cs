﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPJ : MonoBehaviour
{
  
    // instanciacion del PJ y el Gato
    public GameObject PJ;
    public GameObject Gato;
    GameObject newPj;
    GameObject newGato;
    private void Awake()
    {
        //por defecto estan desactivados
        newGato = Instantiate(Gato);
        newPj = Instantiate(PJ);
        newPj.SetActive(false);
        newGato.SetActive(false);
    }
    //cada boton activa su respectivo GameObject y desactiva el otro
    public void spawnPj()
    {
        newGato.SetActive(false);
        newPj.SetActive(true);        
    }
    public void spawnGato()
    {
        newPj.SetActive(false);
        newGato.SetActive(true);
    }
}
