﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    private float speed;
    private float knockback;

    //doy valor random al knockback y a la velocidad
    private void Awake()
    {
        knockback = Random.RandomRange(450f, 650f);
        speed = Random.RandomRange(8f, 15f);
    }
    void Start()
    {   
        //destruyo la bala al pasar x tiempo
        GameObject.Destroy(this.gameObject, 5f);
        test();
    }

    //añado la velocidad dada en el awake a la bala
    void Update()
    {
        this.transform.Translate(Vector2.left * speed * Time.deltaTime);
    }
    //añado el knockback a la bala 
    public void test()
    {        
       this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * knockback);
    }   
    //si la bala colisiona con el escudo , la bala es destruida
    private void OnTriggerEnter2D(Collider2D other)
    {  
        if (other.gameObject.tag == "Escudo")
        {
            Destroy(this.gameObject);           
        }
    }

    //si a bala colisiona con el pj o el Gato, les hace daño y la bala se destruye 
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PjController pj = collision.gameObject.GetComponent<PjController>();
            print("aaa");
            pj.healthSystem.Damage(20);
            Destroy(this.gameObject);
        }
        if (collision.gameObject.tag == "Gato")
        {
            GatoController pj = collision.gameObject.GetComponent<GatoController>();
            print("aaa");
            pj.healthSystem.Damage(20);
            Destroy(this.gameObject);
        }

    }
}
