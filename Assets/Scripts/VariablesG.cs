﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariablesG: MonoBehaviour
{
    private float knockback;
    
    //control del kockback que hacen los ataques del pj y el gato
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "ataqueFlojo")
        {
            knockback = 40f;
            Vector2 diferencia = transform.position - other.transform.position;
            diferencia.Normalize();
            this.GetComponent<Rigidbody2D>().AddForce((diferencia + Vector2.up) * knockback);         

        }
        if (other.tag == "ataqueMedio")
        {
            knockback = 45f;
            Vector2 diferencia = transform.position - other.transform.position;
            diferencia.Normalize();
            this.GetComponent<Rigidbody2D>().AddForce((diferencia+Vector2.up )* knockback);
        }   
        if (other.tag == "ataqueFuerte")
        {
            knockback = 150f;
            Vector2 diferencia = transform.position - other.transform.position;
            diferencia.Normalize();
            this.GetComponent<Rigidbody2D>().AddForce((diferencia + Vector2.up) * knockback);            
        }
        if (other.tag == "ataqueFlojo2")
        {
            print("q");
            knockback = 350f;
            Vector2 diferencia = transform.position - other.transform.position;
            diferencia.Normalize();
            this.GetComponent<Rigidbody2D>().AddForce((diferencia + Vector2.up) * knockback);
        }
        if (other.tag == "ataqueFlojo3")
        {
            print("w");
            knockback = 350f;
            Vector2 diferencia = transform.position - other.transform.position;
            diferencia.Normalize();
            this.GetComponent<Rigidbody2D>().AddForce((diferencia + Vector2.up) * knockback);
        }

    }
}
