﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{   
    //control de scenas 
    public void swapEscena0()
    {
        SceneManager.LoadScene(1);
    }

    public void swapEscena1()
    {
        SceneManager.LoadScene(0);
    }
}
