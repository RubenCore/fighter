﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cano : MonoBehaviour
{
    public GameObject bala;

    //cada x tiempo invoco una bala
    void Start()
    {        
        InvokeRepeating("disparo", 1f , 3f);
    } 
    //instancio una bala
    private void disparo()
    {
        GameObject aux = (GameObject)Instantiate(bala);
        aux.transform.position = this.transform.position;       
    }
}
