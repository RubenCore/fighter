﻿using System;

public class HealthSystem 
{
    //sistema de control de vid
    private int health;
    private int healthMax;
    public event EventHandler OnHealthChange;

    //pones la heal al valor que quieras
    public HealthSystem(int healthMax)
    {
        this.healthMax = healthMax;
        health = healthMax;
    }
    //returna la heal 
    public int GetHealth(){
        return health;
    }
    //devuelve el percent de la heal
    public float GetHealthPercent()
    {
        return (float)health / healthMax;
    }

    //hace daño pasandole un int a la funcion , al pasarle el int a la funcion se actualiza la barra de vida con el evento
    public void Damage(int damageAmount)
    {
        health -= damageAmount;
        if (health < 0)
        {
            health = 0;
        }
        if(OnHealthChange != null)
        {
            OnHealthChange(this, EventArgs.Empty);
        }
    }

    //la funcion heal , cura x cantidad de vida que le pasas por el int y actualiza la barra de vida con el evento
    public void Heal(int healAmount)
    {
        health += healAmount;
        if (health > healthMax)
        {
            health = healthMax;
        }
        if (OnHealthChange != null)
        {
            OnHealthChange(this, EventArgs.Empty);
        }
    }
}

