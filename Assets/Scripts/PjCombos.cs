﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PjCombos : VariablesG
{
    public Animator animator;
    //combos del pj principal
    //q=1 qq=1 qqq=3 
    int comboStatus = 0;
    void Update()
    {
        if (Input.GetKeyDown("q"))
        {
            StartCoroutine(combo("q", 1f));
        }      
        else if (Input.GetKeyDown("e"))
        {
            StartCoroutine(combo("e", 1f));
        }        
    }

    IEnumerator combo(string s, float wait)
    {
        if (comboStatus == 0)//inicio combo 
        {
            if (s == "q")
            {
                //  print("Q");
                animator.SetTrigger("Atack1");
                comboStatus = 1;//q
                yield return new WaitForSeconds(wait);
                if (comboStatus == 1)
                {
                   
                    comboStatus = 0;
                }
            }
            if (s == "e")
            {
                // print("E");
                animator.SetTrigger("Atack1.2");
                comboStatus = 1;//q
                yield return new WaitForSeconds(wait);
                if (comboStatus == 1)
                {
                 
                    comboStatus = 0;
                }
            }
        }
        else if (comboStatus == 1)
        {
            if (s == "q")
            {
                //  print("Q/Q");
                animator.SetTrigger("Atack2");
                comboStatus = 2; //qq
                yield return new WaitForSeconds(wait);
                if (comboStatus == 2)
                {
              
                    comboStatus = 0;
                }
            }
            if (s == "e")
            {
                // print("E/E");
                animator.SetTrigger("Atack2.2");
                comboStatus = 2; //ee
                yield return new WaitForSeconds(wait);
                if (comboStatus == 2)
                {
               
                    comboStatus = 0;
                }
            }
        }
        else if (comboStatus == 2)
        {
            if (s == "q")
            {
              //  print("Q/Q/Q");
                animator.SetTrigger("Atack3");
                comboStatus = 0;//qqq
            }
            if (s == "e")
            {
               // print("E/E/E");
                animator.SetTrigger("Atack4");
                comboStatus = 0;//eee
            }
        }        
    }
}
