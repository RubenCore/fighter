﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saco : MonoBehaviour
{
    //barra de vida
    public HealthBar healthBar;
    HealthSystem healthSystem;
    
    void Start()
    {
        healthSystem = new HealthSystem(100);
        healthBar.Setup(healthSystem);          
    }   
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //si el saco colisiona con la pared o le vaja la vida a 0 vuelve a colocar en su posicion de spawn con la vida a 100
        if (collision.transform.tag == "Pared" || healthSystem.GetHealth() == 0)
        {         
            this.GetComponent<Rigidbody2D>().position = new Vector2(0f, 5f);            
            healthSystem.Heal(100);          
        }       
    }

    //control del daño que recibe el saco al ser golpeado por los ataques del PJ o el Gato
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "ataqueFlojo")
        {
            healthSystem.Damage(10);            
        }
        if (other.tag == "ataqueMedio")
        {
            healthSystem.Damage(20);           
        }
        if (other.tag == "ataqueFuerte")
        {
            healthSystem.Damage(30);           
        }
        if (other.tag == "ataqueFlojo2")
        {
            healthSystem.Damage(45);           
        }
        if (other.tag == "ataqueFlojo3")
        {
            healthSystem.Damage(45);           
        }
    }
}
